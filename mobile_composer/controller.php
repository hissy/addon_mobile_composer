<?php
namespace Concrete\Package\MobileComposer;

use Core;
use URL;
use Route;

class Controller extends \Concrete\Core\Package\Package
{
    /**
     * Package handle.
     */
    protected $pkgHandle = 'mobile_composer';

    /**
     * Required concrete5 version.
     */
    protected $appVersionRequired = '5.7.5';

    /**
     * Package version.
     */
    protected $pkgVersion = '0.1';

    /**
     * Remove \Src from package namespace.
     */
    protected $pkgAutoloaderMapCoreExtensions = true;

    /**
     * Returns the translated package description.
     *
     * @return string
     */
    public function getPackageDescription()
    {
        return t('Add a new page with composer on mobile devices.');
    }

    /**
     * Returns the installed package name.
     *
     * @return string
     */
    public function getPackageName()
    {
        return t('Mobile Composer');
    }

    public function on_start()
    {
        /** @var \Concrete\Core\Application\Service\UserInterface\Menu $menuHelper */
        $menuHelper = Core::make('helper/concrete/ui/menu');
        $menuHelper->addPageHeaderMenuItem('mobile_composer', $this->pkgHandle, array(
            'icon' => 'files-o',
            'label' => t('Add a page'),
            'position' => 'right',
            'href' => URL::to('/ccm/mobile_composer/menu'),
            'linkAttributes' => array(
                'title' => t('Add a Page'),
                'dialog-title' => t('Add a Page'),
                'dialog-modal' => "true",
                'dialog-width' => '100%',
                'dialog-height' => "100%",
                'class' => 'dialog-launch hidden-sm hidden-md hidden-lg'
            )
        ));

        Route::register(
            '/ccm/mobile_composer/menu',
            '\Concrete\Package\MobileComposer\Controller\Dialog\Page\ComposerMobile::menuAction'
        );
        Route::register(
            '/ccm/mobile_composer/add',
            '\Concrete\Package\MobileComposer\Controller\Dialog\Page\ComposerMobile::addAction'
        );
    }
}