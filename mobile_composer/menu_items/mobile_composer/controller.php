<?php
namespace Concrete\Package\MobileComposer\MenuItem\MobileComposer;

use PageType;
use Permissions;

class Controller extends \Concrete\Core\Application\UserInterface\Menu\Item\Controller
{
    public function displayItem()
    {
        $canView = false;
        $list = PageType::getList();
        foreach ($list as $pt) {
            $ptp = new Permissions($pt);
            if ($ptp->canAddPageType()) {
                $canView = true;
            }
        }
        return $canView;
    }
}