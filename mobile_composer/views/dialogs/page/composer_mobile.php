<?php
defined('C5_EXECUTE') or die("Access Denied.");

$form = Core::make('helper/form');

if (count($frequentPageTypes) || count($otherPageTypes)) {?>
    <h4><?=t('New Page')?></h4>
    <div class="list-group ccm-mobile-composer-form">
        <?php foreach($frequentPageTypes as $pt) { ?>
            <a href="#" class="list-group-item page-type" data-ptid="<?php echo $pt->getPageTypeID(); ?>"><?=$pt->getPageTypeDisplayName()?></a>
        <?php } ?>
        <?php foreach($otherPageTypes as $pt) { ?>
            <a href="#" class="list-group-item page-type hidden" data-ptid="<?php echo $pt->getPageTypeID(); ?>"><?=$pt->getPageTypeDisplayName()?></a>
        <?php } ?>
        <?php if (count($frequentPageTypes) && count($otherPageTypes)) { ?>
            <a href="#" class="list-group-item" data-sitemap="show-more"><i class="fa fa-caret-down"></i> <?=t('More')?></a>
        <?php } ?>
    </div>

    <script type="text/javascript">
        $(function() {
            $('.ccm-mobile-composer-form .page-type').on('click', function(e){
                e.preventDefault();
                var ptID = $(this).data('ptid');
                $.ajax({
                    method: "post",
                    url: "<?php echo URL::to('/ccm/mobile_composer/add'); ?>",
                    dataType: "json",
                    data: {
                        ccm_token: "<?php echo Core::make('helper/validation/token')->generate('composer_mobile'); ?>",
                        ptID: ptID
                    },
                    beforeSend: function(r) {
                        jQuery.fn.dialog.showLoader();
                    }
                }).done(function(d){
                    // console.log(d.cID);
                    jQuery.fn.dialog.hideLoader();
                    if (d.redirectURL) {
                        window.location.href = d.redirectURL;
                    } else {
                        jQuery.fn.dialog.open({
                            width: '100%',
                            height: '100%',
                            modal: true,
                            title: '<?php echo t("Composer") ?>',
                            href: '<?php echo URL::to('/ccm/system/panels/details/page/composer'); ?>?cID=' + parseInt(d.cID)
                        });
                    }
                }).fail(function(d){
                    jQuery.fn.dialog.hideLoader();
                    jQuery.fn.dialog.closeTop();
                    ConcreteAlert.error({
                        'message': ccmi18n.generalRequestError,
                        'title': ccmi18n.generalRequestError
                    });
                });
            });
            $('.ccm-mobile-composer-form a[data-sitemap=show-more]').on('click', function(e) {
                e.preventDefault();
                $('.list-group-item.hidden').removeClass('hidden');
                $(this).remove();
            });
        });
    </script>
<?php }

if (count($drafts)) {?>
    <h4><?=t('Page Drafts')?></h4>
    <?php echo $form->select('select-drafts', $drafts); ?>

    <script type="text/javascript">
        $(function() {
            $('#select-drafts').on('change', function(e) {
                e.preventDefault();
                var clink = $(this).val();
                window.location.href = clink;
            });
        });
    </script>
<?php }