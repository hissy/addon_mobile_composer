<?php
namespace Concrete\Package\MobileComposer\Controller\Dialog\Page;

use PageType;
use Permissions;
use Core;
use Concrete\Controller\Backend\Page as BackendPageController;
use Concrete\Core\Page\EditResponse;
use Page;
use RedirectResponse;
use Response;

class ComposerMobile extends \Concrete\Controller\Backend\UserInterface
{
    protected $viewPath = '/dialogs/page/composer_mobile';
    protected $validationToken = 'composer_mobile';
    protected $frequentPageTypes = array();
    protected $otherPageTypes = array();

    protected function canAccess()
    {
        $frequentlyUsed = PageType::getFrequentlyUsedList();
        foreach($frequentlyUsed as $pt) {
            $ptp = new Permissions($pt);
            if ($ptp->canAddPageType()) {
                $this->frequentPageTypes[] = $pt;
            }
        }

        $otherPageTypes = PageType::getInfrequentlyUsedList();
        foreach($otherPageTypes as $pt) {
            $ptp = new Permissions($pt);
            if ($ptp->canAddPageType()) {
                $this->otherPageTypes[] = $pt;
            }
        }

        $canAccess = false;
        if (count($this->frequentPageTypes) || count($this->otherPageTypes)) {
            $canAccess = true;
        }
        return $canAccess;
    }

    public function menuAction()
    {
        $drafts = Page::getDrafts();
        $mydrafts = array(
            '#' => t('Choose a draft')
        );
        foreach($drafts as $dc) {
            $dp = new Permissions($dc);
            if ($dp->canEditPageContents()) {
                $name = ($dc->getCollectionName()) ? $dc->getCollectionName() : t('(Untitled)');
                $date = Core::make('date')->formatDateTime($dc->getCollectionDateAdded(), false);
                $pt = $dc->getPageTypeObject();
                $pagetype = $pt->getPageTypeDisplayName();
                $key = $dc->getCollectionLink();
                $mydrafts[$key] = sprintf('%s (%s) %s', $name, $pagetype, $date);
            }
        }

        $this->set('drafts', $mydrafts);
        $this->set('frequentPageTypes', $this->frequentPageTypes);
        $this->set('otherPageTypes', $this->otherPageTypes);
    }

    public function addAction()
    {
        if ($this->validateAction()) {
            $ptID = $this->post('ptID');
            $pagetype = PageType::getByID(Core::make('helper/security')->sanitizeInt($ptID));
            if (is_object($pagetype)) {
                $ptp = new Permissions($pagetype);
                if ($ptp->canAddPageType()) {
                    $pt = $pagetype->getPageTypeDefaultPageTemplateObject();
                    $d = $pagetype->createDraft($pt);
                    $response = new EditResponse();
                    $response->setMessage(t('Draft created.'));
                    $response->setPage($d);
                    if (!$pagetype->doesPageTypeLaunchInComposer()) {
                        $response->setRedirectURL(Core::getApplicationURL() . '/' . DISPATCHER_FILENAME . '?cID=' . $d->getCollectionID() . '&ctask=check-out-first&' . Core::make('helper/validation/token')->getParameter());
                    }
                    $response->outputJSON();
                }
            }
        }

        return new Response(t('Access Denied'), Response::HTTP_BAD_REQUEST);
    }
}